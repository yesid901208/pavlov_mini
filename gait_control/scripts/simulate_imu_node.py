#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import cv2
import numpy as np
from cv_bridge import CvBridge
from sensor_msgs.msg import Imu
from geometry_msgs.msg import TwistStamped
import time
from tf.transformations import quaternion_from_euler


class SimulateIMU:
    def __init__(self):
        self.debug("INFO", "pavlov_mini_vision_node is running!")
        ####### publisher ##########
        self.IMUPub = rospy.Publisher("/pavlov_mini/imu_data", Imu, queue_size=1)
        ####### subscribers ########
        rospy.Subscriber("/pavlov_mini/vel_cmd", TwistStamped, self.velCmdCB)

        self.yaw_vel = 0
        self.yaw     = 0

        self.timer = time.time()

        self.imu_data = Imu()


    """ call backs functions """
    def velCmdCB(self, data):
        self.yaw_vel = data.twist.angular.z

    """ Code for the main thread of the node """

    def mainThread(self):
        dt = time.time() - self.timer
        self.timer = time.time()

        self.yaw += 0.1*self.yaw_vel * dt

        q = quaternion_from_euler(0.3, 0.0, self.yaw)
        self.imu_data.angular_velocity.x = 0.3
        self.imu_data.angular_velocity.y = 0
        self.imu_data.angular_velocity.z = 0

        self.imu_data.header.stamp = rospy.Time.now()
        self.imu_data.orientation.x = q[0]
        self.imu_data.orientation.y = q[1]
        self.imu_data.orientation.z = q[2]
        self.imu_data.orientation.w = q[3]

        self.IMUPub.publish(self.imu_data)



    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('publish_camera_view_node', anonymous=True)
        rate = rospy.Rate(50)  # 10 Hz
        node = SimulateIMU()

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass

