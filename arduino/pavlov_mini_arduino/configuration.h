#ifndef CONFIGURATION_H
#define CONFIGURATION_H


#define NUM_MOTORS 12
#define PUBLISH_DATA_FREQ 30
#define PUBLISH_INFO_FREQ 1


#define VELOCITY_FILTER  0.01


#define QUATERNION_FILTER 0.0
#define ANG_VEL_FILTER    0.0
#define ACC_FILTER        0.0



/* Set the delay between fresh samples */
#define BNO080_SAMPLERATE_DELAY_MS 5
#define COV_MATRIX_SAMPLE 100

#endif
