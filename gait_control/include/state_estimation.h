//
// Created by andres on 07.11.20.
//

#ifndef STATE_ESTIMATION_H
#define STATE_ESTIMATION_H

#include <sstream>
#include <stdio.h>
#include <iostream>
#include <string>
#include <urdf/model.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/LU>
#include "settings.h"
#include <math.h>
#include "kinematics.h"

#define STATE_DIM 18
#define MEAS_DIM  28

class StateEstimation {
public:
    StateEstimation(Kinematics* kinematics);

    ~StateEstimation();

    void reset();

    void setInitialState(const Eigen::Vector4f &base_quaternion_, float *jointPos);

    void setInput(const Eigen::Vector4f &base_quaternion_, const Eigen::Vector3f &acc_, bool *feet_stance, float deltat);


    void predictState();

    void getFeetFromBaseRotated(std::string leg, const Eigen::Vector3f joint_angles, Eigen::Matrix<float, 3, 1>* feet_pos_from_base_rotated);

    void setMeasurement(const Eigen::Vector3f gyro, float *jointPos, float *jointVel, float *feetHeight_);

    void updateState();

    void getBasePosition(Eigen::Vector3f* base_pos);
    void getBaseVelocity(Eigen::Vector3f* base_vel);
    void getBaseAngularVelocity(Eigen::Vector3f* base_angular_vel);
    void getFeetPosition(std::string leg, Eigen::Vector3f* feet_pos);
    void getBaseVelocityFromBaseRef(Eigen::Vector3f* base_vel);
    void getBaseAngularVelocityFromBaseRef(Eigen::Vector3f* base_ang_vel);
    void getBaseRotationMatrix(Eigen::Matrix3f* base_rot_matrix);
    void getBaseAccelerationFromBaseRef(Eigen::Vector3f* base_acc);
    float getMeanFeetHeight();
    void getCoMPosition(Eigen::Vector3f* com_pos);

private:
    void getFeetPositionCovariance(bool feet_stance, Eigen::Matrix3f* Qfeet);
    float getFeetHeightCovariance(bool feet_stance);


private:
    Kinematics*         kinematics;


    float dt;
    bool *feet_stance;
    float* joint_angles;
    Eigen::Vector4f base_quaternion;
    Eigen::Vector3f angular_vel;
    Eigen::Matrix<float, STATE_DIM, 1> x;
    Eigen::Matrix<float, STATE_DIM, STATE_DIM> P;
    Eigen::Matrix<float, STATE_DIM, STATE_DIM> Q;

    Eigen::Matrix3f base_rotation_matrix;
    Eigen::Matrix<float, 3, 1> acc;

    Eigen::Matrix<float, MEAS_DIM, 1> z;
    Eigen::Matrix<float, MEAS_DIM, STATE_DIM> H;
    Eigen::Matrix<float, MEAS_DIM, MEAS_DIM> R;

    Eigen::Matrix3f O;
    Eigen::Matrix3f I;

    Eigen::Matrix<float, STATE_DIM, STATE_DIM> I_state_dim;
};

#endif