//
// Created by andres on 13.10.20.
//

#include "../include/feet_trajectory.h"
FeetTrajectory::FeetTrajectory(std::string name){
    this->stop_request_state = NON_STOP_REQUEST;
    this->walking           = true;


    this->leg_name = name;
    this->phase_offset = 0;
    this->leg_phase    = 0;
    this->feet_state_phase = 0;
    feet_state      = STANCE;
    feet_prev_state = STANCE;

    this->initial_pos << 0,0,0;
    this->current_pos << 0,0,0;
    this->middle_pos  << 0,0,0;
    this->final_pos   << 0,0,0;
    this->feet_pos_offset << 0,0,0;
    this->prev_feet_vel << 0,0,0;
    this->height = 0;
    this->set_initial_pos = false;

    this->max_leg_displacement = 0.1;

    this->integrated_feet_pos << 0, 0, 0;

    timer = ros::Time::now();

    this->use_integrated_pos = false;

}


FeetTrajectory::~FeetTrajectory(){

}

void FeetTrajectory::setTrajectoryParams(double period, double prop_time_swing, double phase_offset, float height,  float feet_offset_x, float feet_offset_y, float base_offset_x, float base_offset_y){
    this->period          = period;
    this->prop_time_swing = prop_time_swing;
    this->phase_offset    = phase_offset;
    this->height          = height;

    this->feet_pos_offset << feet_offset_x, feet_offset_y, 0;
    this->base_pos_offset << base_offset_x, base_offset_y, 0;
    if ((this->leg_name == "fr")||(this->leg_name == "br")) this->feet_pos_offset.y() = -this->feet_pos_offset.y();
    if ((this->leg_name == "br")||(this->leg_name == "bl")) this->feet_pos_offset.x() = -this->feet_pos_offset.x();

}

void FeetTrajectory::setWalking(bool walking){
    if (walking == this->walking) return;

    if (walking){
        this->walking = true;
        this->stop_request_state = NON_STOP_REQUEST;
        this->feet_state        = STANCE;
        this->leg_phase     = 0;
        this->feet_state_phase = 0;
        this->set_initial_pos  = false;
        this->timer = ros::Time::now();
        this->current_pos << 0,0,0;

    }
    else if ((!walking) && (this->stop_request_state == NON_STOP_REQUEST)){
        this->stop_request_state = REQUESTED_TO_STOP;
    }
}


bool FeetTrajectory::predictFeetState(double current_main_phase, double dt, const Eigen::Vector3f &des_base_vel, Eigen::Vector3f* predicted_feet_pos){
    if (not this->walking){
        predicted_feet_pos->operator=( feet_pos_offset ); //this->current_pos );
        return true;
    }


    double phase_    = current_main_phase + dt + this->phase_offset;
    double leg_phase_ = std::fmod(phase_, 1.0);

    bool leg_stance_ = 1;

    if (leg_phase_ < prop_time_swing){   // FEET IS SWING, NO PREDUCTION DURING SWING PERIOD
        leg_stance_ = 0;
        predicted_feet_pos->operator=(  feet_pos_offset - base_pos_offset );
        return leg_stance_;
    }


    Eigen::Vector3f feet_dist = des_base_vel * period*(1 - prop_time_swing); // total distance that the feet will displace in stance
    double t = ((leg_phase_ - prop_time_swing)/(1 - prop_time_swing)) - 0.5; // parameter for time in the range [-0.5, 0.5]
    predicted_feet_pos->operator=(  feet_pos_offset - base_pos_offset + feet_dist * t);

    return leg_stance_;
}


void FeetTrajectory::getFeetState(double main_phase){
    double phase_ = main_phase + this->phase_offset;
    this->leg_phase = std::fmod(phase_, 1.0);


    if (leg_phase < prop_time_swing){
        feet_state = SWING;

        feet_state_phase = leg_phase/prop_time_swing;

        if ((leg_phase < 0.1*prop_time_swing)&&(this->stop_request_state == REQUESTED_TO_STOP)){



            this->stop_request_state = MADE_LAST_STEP;
        }

    }
    else{
        feet_state = STANCE;
        if (feet_prev_state == SWING){
            this->integrated_feet_pos << 0,0,0;
        }

        feet_state_phase = (leg_phase - prop_time_swing)/(1 - prop_time_swing);



        if(this->stop_request_state == MADE_LAST_STEP){

            this->stop_request_state = NON_STOP_REQUEST;
            this->walking = false;
        }

    }

    feet_prev_state = feet_state;
}


void FeetTrajectory::makeStep(const Eigen::Vector3f &leg_des_dir, const Eigen::Vector3f &des_base_vel, const Eigen::Vector3f &base_vel, double main_phase, float base_height, Eigen::Vector3f *feet_pos){
    double dt = (ros::Time::now() - timer).toSec();
    timer = ros::Time::now();


    //Eigen::Vector3f leg_ref_vel_des_nonz, base_vel_nonz;
    //leg_ref_vel_des_nonz << leg_ref_vel_des.x(), leg_ref_vel_des.y(), 0;

    Eigen::Vector3f leg_des_vel, leg_des_vel_nonz, base_vel_nonz;
    leg_des_vel = leg_des_dir;
    leg_des_vel_nonz << leg_des_vel.x(), leg_des_vel.y(), 0;
    base_vel_nonz << base_vel.x(), base_vel.y(), 0;


    if (!this->walking){

        //this->current_pos = this->feet_pos_offset; // + leg_des_dir;

        this->current_pos = this->current_pos +  leg_des_vel*dt;
        feet_pos->operator=(  this->current_pos  );

        return;
    }




    this->getFeetState(main_phase);


    if (this->feet_state == STANCE){
        this->integrated_feet_pos = this->integrated_feet_pos + leg_des_vel*dt;
        this->current_pos = this->current_pos + leg_des_vel*dt;
        this->prev_feet_vel = leg_des_vel_nonz;
        //this->current_pos = this->current_pos + leg_ref_vel_des_nonz*dt; // - (base_vel_nonz - (leg_ref_vel_des_nonz))*dt;
        this->set_initial_pos = false;

        if (this->leg_name == "fr"){
            //std::cout << "eeeeee "  << leg_des_vel.x() << " " << leg_des_vel.y() << " " << leg_des_vel.z() << std::endl;
            //std::cout << leg_des_vel.norm() << std::endl;

        }
    }
    else{
        if (!this->set_initial_pos){
            this->initial_pos.operator=(this->current_pos);
            //this->initial_pos(2) = 0;
            this->set_initial_pos = true;
        }

        if (this->feet_state_phase < 0.1){                      // change final position only at the beginning of the step

            if (this->use_integrated_pos){
                this->final_pos << feet_pos_offset - this->base_pos_offset - this->integrated_feet_pos/2 - ( base_vel_nonz - des_base_vel)*(pow(height/9.8, 0.5));
            }
            else{
                this->final_pos << feet_pos_offset - this->base_pos_offset + des_base_vel*(period*(1-prop_time_swing)) - ( base_vel_nonz - des_base_vel)*(pow(height/9.8, 0.5));
            }



            //this->final_pos = feet_pos_offset + base_pos_offset -  this->prev_feet_vel*(period*(1-prop_time_swing)) + ( base_vel_nonz + this->prev_feet_vel)*(pow(0.15/9.8, 0.5));
            //this->final_pos << this->final_pos.x(), this->final_pos.y(), this->current_pos.z();
            //std::cout << "EEEEEEEEEEEEEEEEEEEEEEEE" << this->final_pos.x() << " " << this->final_pos.y() << " " << this->final_pos.z() << std::endl;

            if (this->leg_name == "fr"){

                //std::cout << des_base_vel.x() << " " << des_base_vel.y() << " " << des_base_vel.z() << std::endl;
                //std::cout << "final pos: "    << this->final_pos.x() << " " << this->final_pos.y() << " " << this->final_pos.z() << std::endl << std::endl  ;

            }
        }

        Eigen::Vector3f middle_pos;
        middle_pos    = this->initial_pos + (this->final_pos - this->initial_pos).operator/(2);
        middle_pos(2) = middle_pos(2) + height;
        //middle_pos(2) -= 0.02;
        double t = (this->leg_phase/prop_time_swing)*PI;
        t = 1- 0.5*(cos(t) + 1);  // Use S curve (cos(t)) for acceleration
        float x = pow(1 - t, 2)*this->initial_pos(0)+ 2*t*(1-t)*middle_pos(0) + pow(t,2)*this->final_pos(0);
        float y = pow(1 - t, 2)*this->initial_pos(1) + 2*t*(1-t)*middle_pos(1) + pow(t,2)*this->final_pos(1);
        float z = pow(1 - t, 2)*this->initial_pos(2) + 2*t*(1-t)*middle_pos(2) + pow(t,2)*this->final_pos(2);

        this->current_pos << x,y,z;
    }

    feet_pos->operator=(this->current_pos);
}




float FeetTrajectory::getWeightingFactor(float sigma1, float sigma2, float sigma1_, float sigma2_, float offset){

    float k_stance = offset + 3*sigma1*pow(1-this->feet_state_phase,2)* this->feet_state_phase + 3*sigma2*(1-this->feet_state_phase)*pow(this->feet_state_phase, 2);
    float k_swing  = offset - 3*sigma1_*pow(1-this->feet_state_phase,2)* this->feet_state_phase - 3*sigma2_*(1-this->feet_state_phase)*pow(this->feet_state_phase, 2);

    if (k_stance > 1) k_stance = 1;
    if (k_swing < 0) k_swing = 0;

    float factor;
    if (this->feet_state == STANCE) {factor =  k_stance;}
    else{ factor =  k_swing;}

    return factor;
}


bool FeetTrajectory::getStance(){
    if (this->feet_state == STANCE){
        return 1;
    }
    else{
        return 0;
    }
}






